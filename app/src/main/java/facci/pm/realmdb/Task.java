package facci.pm.realmdb;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

public class Task extends RealmObject {

    @Required
    private String nombres;
    private String apellidos;
    private String edad;
    private String cedula;
    private String ocupacion;
    @Required
    private String status = TaskStatus.Open.name();

    public Task(String _nombres, String _apellidos, String _edad, String _cedula, String _ocupacion) { this.nombres = _nombres; this.apellidos = _apellidos; this.edad = _edad; this.cedula = _cedula; this.ocupacion= _ocupacion;}
    public Task() {}

    public void setStatus(TaskStatus status) { this.status = status.name(); }
    public String getStatus() { return this.status; }

    public String getNombres() { return nombres; }
    public void setNombres(String nombres) { this.nombres = nombres; }

    public String getApellidos() { return apellidos; }
    public void setApellidos(String apellidos) { this.apellidos = apellidos; }

    public String getEdad() { return edad; }
    public void setEdad(String edad) { this.edad = edad; }

    public String getCedula() { return cedula; }
    public void setCedula(String cedula) { this.cedula = cedula; }

    public String getOcupacion() { return ocupacion; }
    public void setOcupacion(String ocupacion) { this.ocupacion = ocupacion; }

}
